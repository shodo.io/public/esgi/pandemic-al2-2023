package fr.esgi.pandemic.domain.network;

import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public record Network(Map<CityName, City> network,
                      Map<CityName, List<CityName>> links) {

    public Network() {
        this(new HashMap<>(), new HashMap<>());
    }

    public void put(CityName name, City city) {
        network.put(name, city);
    }

    public City get(CityName name) {
        return network.get(name);
    }
}

