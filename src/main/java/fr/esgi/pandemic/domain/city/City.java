package fr.esgi.pandemic.domain.city;

public record City(CityName cityName, int infectionMarker) {

    public static final int MAXIMUM_INFECTION_TIME = 3;

    public City infect() {
        if (infectionMarker == MAXIMUM_INFECTION_TIME) {
            return this;
        }
        return new City(cityName, infectionMarker + 1);
    }
}
