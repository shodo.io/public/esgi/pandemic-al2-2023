package fr.esgi.pandemic.domain.city;

public enum CityName {
    PARIS, ESSEN, MILAN, LONDON
}
