package fr.esgi.pandemic.context;

import fr.esgi.pandemic.domain.network.Network;

public record TestContext(Network network) {

    public TestContext() {
        this(new Network());
    }
}
