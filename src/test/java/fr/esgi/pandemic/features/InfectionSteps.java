package fr.esgi.pandemic.features;

import fr.esgi.pandemic.context.TestContext;
import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Lists;

import java.util.List;
import java.util.stream.Collectors;


public class InfectionSteps {
    private final TestContext ctx;

    public InfectionSteps(TestContext context) {
        this.ctx = context;
    }

    @ParameterType("PARIS|LONDON|MILAN|ESSEN")
    public City city(String cityName) {
        CityName name = CityName.valueOf(cityName);
        City city = ctx.network().get(name);
        if (city == null) {
            ctx.network().put(name, new City(name, 0));
        }
        return ctx.network().get(name);
    }

    @ParameterType(".*")
    public List<CityName> cities(String cityNames) {
        return Lists.newArrayList(cityNames.split(",")).stream().map(CityName::valueOf).collect(Collectors.toList());
    }

    @Given("{city} has not been infected")
    public void cityHasNotBeenInfected(City city) {
        ctx.network().put(city.cityName(), new City(city.cityName(), 0));
    }

    @When("{city} is infected")
    public void cityIsInfected(City city) {
        ctx.network().put(city.cityName(), city.infect());
    }

    @Then("{city} should have {int} marker(s)")
    public void cityShouldHaveMarker(City city, int marker) {
        Assertions.assertThat(city.infectionMarker()).isEqualTo(marker);
    }

    @Given("{city} has been infected {int} time(s)")
    public void parisHasBeenInfectedTime(City city, int infectionTimes) {
        City infectedCity = city;
        for (int i = 0; i < infectionTimes; i++) {
            infectedCity = infectedCity.infect();
        }
        ctx.network().put(infectedCity.cityName(), infectedCity);
    }
}
