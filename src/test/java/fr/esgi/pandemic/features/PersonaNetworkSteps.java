package fr.esgi.pandemic.features;

import fr.esgi.pandemic.context.TestContext;
import fr.esgi.pandemic.domain.city.City;
import fr.esgi.pandemic.domain.city.CityName;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;

import java.util.List;

import static fr.esgi.pandemic.domain.city.CityName.*;
import static org.assertj.core.api.Assertions.assertThat;

public class PersonaNetworkSteps {

    private final TestContext ctx;

    public PersonaNetworkSteps(TestContext ctx) {
        this.ctx = ctx;
    }

    @Given("occidental Network")
    public void occidentalNetwork() {

        ctx.network().put(PARIS, new City(PARIS, 0));
        ctx.network().put(LONDON, new City(LONDON, 0));
        ctx.network().put(ESSEN, new City(ESSEN, 0));
        ctx.network().put(MILAN, new City(MILAN, 0));

        ctx.network().links().put(PARIS, List.of(LONDON, ESSEN));
        ctx.network().links().put(LONDON, List.of(PARIS, ESSEN));
        ctx.network().links().put(ESSEN, List.of(PARIS, LONDON, MILAN));
        ctx.network().links().put(MILAN, List.of(ESSEN));
    }

    @Then("{city} should be linked to {cities}")
    public void cityShouldBeLinkedToCities(City city, List<CityName> linkedCities) {
        assertThat(ctx.network().links().get(city.cityName())).containsExactlyInAnyOrderElementsOf(linkedCities);
    }
}
