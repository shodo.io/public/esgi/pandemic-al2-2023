Feature: City Infection

  Rule: When a city is infected a marker should be added on it

    Scenario: First Infection
      Given PARIS has not been infected
      When PARIS is infected
      Then PARIS should have 1 marker

    Scenario: Second Infection
      Given PARIS has been infected 1 time
      When PARIS is infected
      Then PARIS should have 2 markers

    Scenario: Third Infection
      Given PARIS has been infected 2 times
      When PARIS is infected
      Then PARIS should have 3 markers

  Rule: a marker should not be added on city's fourth infection

    Scenario: Fourth Infection
      Given LONDON has been infected 3 times
      When LONDON is infected
      Then LONDON should have 3 markers